package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class Turn {
    List<String> killedViking = new ArrayList<>();
    List<String> destroyedIsland = new ArrayList<>();

    public void changePositionVikings(Map<String, String> positionVikings, String chosenPath, String nameViking1, List<String> movedVikings) {
        Map<String, String> mapTurn = new HashMap<>();
        List<String> islandDestroyed = new ArrayList<>();

        for (Map.Entry<String, String> entry3 : positionVikings.entrySet()) {

            if (entry3.getValue().equals(chosenPath) && !killedViking.contains(entry3.getKey())) {

                System.out.println(entry3.getKey() + " и " + nameViking1 + " поубивали друг друга");
                System.out.println("АГР!!! На " + chosenPath + " уничтожен маяк, благодаря " + nameViking1 + " и " + entry3.getKey());
                destroyedIsland.add(chosenPath);
                killedViking.add(nameViking1);
                killedViking.add(entry3.getKey());
            }
        }
    }


    public void turn(Map<String, List<String>> currentMap, Map<String, String> positionVikings) {

        Map<String, String> mapTurn = new HashMap<>();
        for (int count = 1; count <= 10; count++) {
            System.out.println("День " + count);
            for (Map.Entry<String, List<String>> entry1 : currentMap.entrySet()) {
                List<String> movedVikings = new ArrayList<>();
                if (destroyedIsland.contains(entry1.getKey())) continue;
                List<String> str = entry1.getValue().stream().filter(x -> !destroyedIsland.contains(x)).collect(Collectors.toList());
                String island = entry1.getKey();
                if (str.size() != 0) {


                    for (Map.Entry<String, String> entry2 : positionVikings.entrySet()) {


                        if (entry2.getValue().equals(island) && !destroyedIsland.contains(island) && !killedViking.contains(entry2.getKey())) {
                            Random R = new Random();

                            String chosenPath = str.get(R.nextInt(str.size()));
                            System.out.println(entry2.getKey() + " приплыл на " + chosenPath);

                            changePositionVikings(positionVikings, chosenPath, entry2.getKey(), movedVikings);


                        }
                        movedVikings.add(entry2.getKey());
                    }
                } else System.out.println("На " + entry1.getKey() + " нет связи с другими островами.");

            }


        }
        System.out.println("Прошло 10000 дней, и у викингов закончилась провизия");


    }

}

