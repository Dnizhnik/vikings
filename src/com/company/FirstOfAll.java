package com.company;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import java.io.File;


public class FirstOfAll extends Main {
    private static int ships;

    public Map<String, List<String>> firstOfAll() throws IOException {

        FileWriter fw = new FileWriter("src/com/company/vikingswar.txt");
        Map<String, List<String>> currentMap = new HashMap<>();
        currentMap.put("Остров1", List.of("Остров2", "Остров3", "Остров6", "Остров4"));
        currentMap.put("Остров2", List.of("Остров1"));
        currentMap.put("Остров3", List.of("Остров1"));
        currentMap.put("Остров4", List.of("Остров1", "Остров5"));
        currentMap.put("Остров5", List.of("Остров6", "Остров4"));
        currentMap.put("Остров6", List.of("Остров1", "Остров5"));

        currentMap.forEach((key, value) -> {
            try {
                fw.write(key + value + System.lineSeparator());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        fw.flush();
        fw.close();

        return currentMap;

    }

    public Map<String, String> secondOFAll(Map<String, List<String>> currentMap) {
        Set<String> keys = currentMap.keySet();
        String[] Arr = keys.toArray(new String[0]);
        Map<String, String> positionVikings = new HashMap<>();
        Main m = new Main();
        ships = m.getShips();

        for (int k = 1; k <= ships; k++) {

            String keyValue = Arr[k];
            keyValue = keyValue.replaceAll("\\s","");
              String name = "Викинг" + k;

            positionVikings.put(name, keyValue);

        }

        for (Map.Entry entry : positionVikings.entrySet()) {
            System.out.println(entry.getKey() + " расположился на "
                    + entry.getValue());
        }
        return positionVikings;

    }
}




